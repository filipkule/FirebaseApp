package service;

import com.google.firebase.auth.FirebaseUser;

/**
 * Created by filip on 6/4/2017.
 */

public class SharedStorage {

    private static SharedStorage instance;

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    private FirebaseUser user;

    public static SharedStorage getInstance() {
        if (instance == null) {
            instance = new SharedStorage();
        }
        return instance;
    }

}
