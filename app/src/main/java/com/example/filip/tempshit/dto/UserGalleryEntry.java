package com.example.filip.tempshit.dto;

/**
 * Created by filip on 6/4/2017.
 */

public class UserGalleryEntry {

    public String url;
    public String name;
    public String storageUid;

}
