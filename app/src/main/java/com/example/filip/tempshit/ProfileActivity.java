package com.example.filip.tempshit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;

import service.SharedStorage;

public class ProfileActivity extends AppCompatActivity {

    private Button next;
    private Button prev;
    private WebView currentEntry;
    private TextView currentNo;
    private TextView currentName;

    private List<Object> gallery;

    private int currentEntryNo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        next = (Button) findViewById(R.id.btn_profile_next);
        prev = (Button) findViewById(R.id.btn_profile_prev);
        currentEntry = (WebView) findViewById(R.id.currentEntry);
        currentNo = (TextView) findViewById(R.id.currentNo);
        currentName = (TextView) findViewById(R.id.currentName);

        currentName.setText("");
        currentNo.setText("");

        fetchGalleryData();

        final ProfileActivity profileActivity = this;
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileActivity.currentEntryNo < gallery.size() - 1) {
                    profileActivity.currentEntryNo++;
                    profileActivity.draw();
                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileActivity.currentEntryNo != 0) {
                    profileActivity.currentEntryNo--;
                    profileActivity.draw();
                }
            }
        });

    }

    private void fetchGalleryData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final SharedStorage sharedStorage = SharedStorage.getInstance();
        final DatabaseReference dbReference = database.getReference("gallery" + sharedStorage.getUser().getUid());
        final ProfileActivity profileActivity = this;
        dbReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Object> gallery = (List<Object>) dataSnapshot.getValue();
                profileActivity.gallery = gallery;
                profileActivity.draw();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void draw() {
        currentEntry.getSettings().setUseWideViewPort(true);
        currentEntry.getSettings().setLoadWithOverviewMode(true);

        HashMap entry = (HashMap) gallery.get(currentEntryNo);
        currentName.setText(entry.get("name").toString());
        currentNo.setText((currentEntryNo + 1) + " / " + gallery.size());
        currentEntry.setWebViewClient(new WebViewClient());
        currentEntry.loadUrl(entry.get("url").toString());
    }
}
