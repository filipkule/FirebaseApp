package com.example.filip.tempshit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import service.SharedStorage;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button btnLogin;
    private Button btnRegister;
    private EditText inputMail;
    private EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btn_register);


        final LoginActivity loginActivity = this;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMail = (EditText) findViewById(R.id.input_mail);
                inputPassword = (EditText) findViewById(R.id.input_password);
                String mail = inputMail.getText().toString();
                String password = inputPassword.getText().toString();
                loginActivity.loginAction(mail, password, v);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginActivity.changeToRegister();
            }
        });
    }

    private void loginAction(String mail, String password, final View view) {
        final LoginActivity loginActivity = this;
        mAuth.signInWithEmailAndPassword(mail, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            SharedStorage sharedStorage = SharedStorage.getInstance();
                            sharedStorage.setUser(user);

                            loginActivity.changeToMain();
                        } else {

                        }
                    }
                });
    }

    private void changeToRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void changeToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
